﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchLighter : MonoBehaviour
{
    public void Light()
    {
        transform.parent.GetComponent<ParticleSystem>().Play();
        UI.ui_bar.AddLight();

        gameObject.SetActive(false);   
    }
}
