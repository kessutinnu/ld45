﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] private float speed = 2;
    public int damage = 1; 
    private Transform[] heads;
    private Rigidbody2D body;

    private void Start()
    {
        heads = GetComponentsInChildren<Transform>();
        body = GetComponent<Rigidbody2D>();
        transform.right *= Mathf.Sign(transform.localScale.x);
        transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, 1);
    }
    // Update is called once per frame
    void Update()
    {
        body.position += new Vector2 (Time.deltaTime * speed * transform.right.x, 0);
        foreach(Transform head in heads)
        {
            var hit = Physics2D.Raycast(head.position, transform.right, 0.2f, LayerMask.GetMask("Default"));
            if (hit.collider != null)
            {
                transform.right *= -1;
                break;
            }
        }

    }




}
