﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Player : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private float jumpspeed;
    [SerializeField] private float grow_speed;
    [SerializeField] private GameObject nothing_prefab;
    [SerializeField] private int health;

    private Rigidbody2D body;
    private Tilemap erasables;
    private float radius;
    private Vector3 center;
    private Transform[] feet;
    private float scale;
    private GameObject nothing;
    
    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        erasables = GameObject.FindGameObjectWithTag("erasableTM").GetComponent<Tilemap>();
        nothing = Instantiate(nothing_prefab);
        nothing.SetActive(false);
        feet = GetComponentsInChildren<Transform>();
        scale = transform.localScale.x;
        UI.ui_bar.ShowLives(health);
        
    }

    // Update is called once per frame
    void Update()
    {

        var jump = (Input.GetKeyDown("space") && IsGrounded()) ? jumpspeed : 0;
        var horizontal = Input.GetAxis("Horizontal");
        body.velocity = new Vector2(horizontal*speed, body.velocity.y + jump);
        var facing = horizontal != 0 ? scale * Mathf.Sign(Input.GetAxis("Horizontal")) : transform.localScale.x ;
        transform.localScale = new Vector3(facing, scale, 1);

        if (Input.GetAxis("Erase")==1)
        {
            if(radius == 0)
            {
                center = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                center.z = 0;
                Cursor.visible = false;
                nothing.SetActive(true);
                nothing.transform.position = center;
                nothing.transform.localScale = new Vector3(0,0);
            }
            
            radius += grow_speed * Time.deltaTime;
            radius = Mathf.Min(3.5f,radius);
            nothing.transform.localScale = new Vector3(radius*2, radius*2);
            var angle = Mathf.Rad2Deg * 0.7f/radius;
            for (float i=0; i< 360; i += angle)
            {
                var pos = erasables.WorldToCell(center + Quaternion.AngleAxis(i, Vector3.forward) * ((radius - 0.3f) * Vector3.up));           
                erasables.SetTile(pos, null);
            }

        }
        if (Input.GetMouseButtonUp(0))
        {
            radius = 0;
            Cursor.visible = true;
            nothing.SetActive(false);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Nothing"))
        {
            Losehealth();
            //send the nothing away
            body.position += new Vector2(Mathf.Sign(transform.position.x - center.x) * 0.3f, 0);
            body.velocity += new Vector2(0, 2f);
            center.z = 11;
            center.y = 1000;
            nothing.transform.position = center;
            return;
        }
        if (collision.tag.Equals("Finish"))
        {
            UI.ui_bar.EndGame(true);
        }
        TorchLighter torch;
        if (collision.TryGetComponent(out torch))
        {
            torch.Light();
        }
    }

    private bool IsGrounded()
    {
        foreach (Transform f in feet){
            RaycastHit2D hit = Physics2D.Raycast(f.position, Vector2.down, 0.2f, LayerMask.GetMask("Default"));
            if (hit.collider != null)
            {
                return true;
            }
        }
        return false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        EnemyMovement enemy;
        if (collision.gameObject.TryGetComponent(out enemy))
        {
            Losehealth(enemy.damage);
            Destroy(enemy.gameObject);
            
        }
    }

    private void Losehealth(int damage = 1)
    {
        health -= damage;
        UI.ui_bar.ShowLives(Mathf.Max(0,health));
        if (health <= 0)
        {
            UI.ui_bar.EndGame(false);
        }
    }


}
