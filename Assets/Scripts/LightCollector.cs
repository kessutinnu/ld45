﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LightCollector : MonoBehaviour
{
    public static LightCollector instance;
    private Dictionary<int, Vector2Int> lights;
    [SerializeField] private GameObject levelButton;

    void Awake()
    {
        if (instance != null) Destroy(gameObject);
        else
        {
            DontDestroyOnLoad(gameObject);
            lights = new Dictionary<int, Vector2Int>();
            SceneManager.sceneLoaded += OnSceneLoaded;
            instance = this;
        }
    }

    public void Addlevel(int index, int total, int collected = 0)
    {

        if (lights.ContainsKey(index) && lights[index].x > collected) return;
        lights[index] = new Vector2Int(collected,total);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {

        if (scene.name.Equals("MainMenu"))
        {
            Transform panel = GameObject.Find("LevelSelect").transform;
            Debug.Log(panel.childCount);

            for (int i = 1; i <= lights.Count; i++)
            {
                var button = Instantiate(levelButton, panel);
                button.transform.GetChild(0).GetComponent<Text>().text = "" + (i - 1);
                button.transform.GetChild(1).GetComponent<Text>().text = lights[i][0] + "/" + lights[i][1];
                int x = new int();
                x = i;
                button.GetComponent<Button>().onClick.AddListener(delegate { SceneManager.LoadScene(x, LoadSceneMode.Single); });

            }
        }
   
    }


}
