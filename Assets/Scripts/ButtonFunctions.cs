﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonFunctions: MonoBehaviour
{
    public void RestartGame()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }
    public void MainMenu()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

}
