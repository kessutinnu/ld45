﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public static UI ui_bar;
    private Text livesText;
    private Text lightsText;
    private int lightsTotal;
    private int lit=0;

    // Start is called before the first frame update
    void Start()
    {
        livesText = transform.Find("Lives").GetComponent<Text>();
        lightsText = transform.Find("LightCount").GetComponent<Text>();
        lightsTotal = GameObject.Find("Lights").transform.childCount;
        lightsText.text = "Lights 0/" + lightsTotal;
        ui_bar = this;
        LightCollector.instance.Addlevel(SceneManager.GetActiveScene().buildIndex, lightsTotal);
    }

    public void ShowLives(int lives)
    {
        livesText.text = "Lives: " + lives;
    }

    public void AddLight()
    {
        lit += 1;
        lightsText.text = "Lights "+ lit +"/" + lightsTotal;
    }

    public void EndGame(bool win)
    {
        if (win)
        {
            LightCollector.instance.Addlevel(SceneManager.GetActiveScene().buildIndex, lightsTotal,lit);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        }
        else
        {
            Restart();
        }
    }

    public void ShowMessage(string message)
    {
        lightsText.text = message;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }


}
