﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMessage : MonoBehaviour
{
    [SerializeField] private string message;

    public void PlayMessage()
    {
        UI.ui_bar.ShowMessage(message);
        gameObject.SetActive(false);
    }
}
