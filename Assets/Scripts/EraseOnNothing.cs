﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EraseOnNothing : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Nothing"))
        {
            Destroy(gameObject);
        }
    }
}
