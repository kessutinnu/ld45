﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialListener : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Tutorial"))
        {
            collision.gameObject.GetComponent<TutorialMessage>().PlayMessage();
        }
    }
}
