﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Intro : MonoBehaviour
{

    private GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Character");
        player.SetActive(false);
    }

    private void Update()
    {
        if (Input.anyKeyDown)
        {
            GetComponentInChildren<Text>().text = "You are indeed faithful and for that I give you Nothing. Use it well!";
            transform.GetChild(1).gameObject.SetActive(false);
            Invoke("EndIntro", 4);
        }
    }

    private void EndIntro()
    {
        player.SetActive(true);
        Destroy(gameObject);
    }
}
